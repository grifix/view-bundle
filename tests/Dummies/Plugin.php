<?php

declare(strict_types=1);

namespace Grifix\ViewBundle\Tests\Dummies;

final class Plugin
{
    public function __construct()
    {
    }

    public function getValue(): string
    {
        return 'test';
    }
}
