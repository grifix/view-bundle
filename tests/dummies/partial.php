<?php

declare(strict_types=1);

use Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */
?>

<ul class="partial">
    <?php
    foreach ($this->getVar('items') as $item): ?>
        <li>
            <?= $this->entitle($item) ?>
        </li>
    <?php
    endforeach ?>
</ul>
