<?php

declare(strict_types=1);

use Grifix\View\TemplateInterface;

/** @var $this TemplateInterface */

$this->inherits('layout.php');
?>

<?php $this->startSlot('content'); ?>
<h1>List</h1>
<?= $this->renderPartial('partial.php', ['items' => ['one', 'two', 'three']]) ?>
<?php $this->endSlot(); ?>
