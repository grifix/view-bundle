<?php

declare(strict_types=1);

use Grifix\View\TemplateInterface;
use Grifix\ViewBundle\Tests\Dummies\Plugin;

/** @var $this TemplateInterface */
?>
<?=$this->getPlugin(Plugin::class)->getValue()?>
<div id="content">
    <?php $this->startSlot('content'); ?>
    <?= $this->getVar('content') ?>
    <?php $this->endSlot(); ?>
</div>
<div id="footer">
    <?php $this->startSlot('footer'); ?>
    <?= $this->getVar('footer') ?>
    <?php $this->endSlot(); ?>
</div>
