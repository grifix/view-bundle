<?php

declare(strict_types=1);

namespace Grifix\ViewBundle\Tests;

use Grifix\View\ViewFactoryInterface;
use Grifix\ViewBundle\GrifixViewBundle;
use Nyholm\BundleTest\TestKernel;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

final class ViewBundleTest extends KernelTestCase
{
    protected static function getKernelClass(): string
    {
        return TestKernel::class;
    }

    protected static function createKernel(array $options = []): KernelInterface
    {
        /**
         * @var TestKernel $kernel
         */
        $kernel = parent::createKernel($options);
        $kernel->addTestBundle(GrifixViewBundle::class);
        $kernel->addTestConfig(__DIR__ . '/config.yaml');
        $kernel->handleOptions($options);

        return $kernel;
    }

    public function testItWorks(): void
    {
        $kernel = self::bootKernel();

        $viewFactory = $kernel->getContainer()->get(ViewFactoryInterface::class);
        $html        = $viewFactory->createView('template.php')->render([
            'footer' => 'footer',
            'items'  => [
                'one',
                'two',
                'three'
            ]
        ]);
        self::assertEquals(
            'test<div id="content"> <h1>List</h1> <ul class="partial"> <li> one </li> <li> two </li> <li> three </li> </ul></div> <div id="footer"> footer </div>',
            $html
        );
    }
}
