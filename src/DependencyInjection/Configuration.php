<?php

declare(strict_types=1);

namespace Grifix\ViewBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('grifix_view');
        $treeBuilder->getRootNode()
                    ->children()
                        ->scalarNode('view_dir')->cannotBeEmpty()->end()
                        ->arrayNode('plugins')->scalarPrototype()->end()
                    ->end();


        return $treeBuilder;
    }
}
