<?php

declare(strict_types=1);

namespace Grifix\ViewBundle;

use Grifix\View\AssetPackage\AssetSourceInterface;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;

final class SymfonyAssetSource implements AssetSourceInterface
{
    private Package $package;

    public function __construct()
    {
        $this->package = new Package(new EmptyVersionStrategy());
    }

    public function getAssetUrl(string $path): string
    {
        return $this->package->getUrl($path);
    }
}
