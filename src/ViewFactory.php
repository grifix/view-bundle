<?php

declare(strict_types=1);

namespace Grifix\ViewBundle;

use Grifix\View\ViewFactory as BaseViewFactory;
use Grifix\View\ViewFactoryInterface;
use Grifix\View\ViewInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class ViewFactory implements ViewFactoryInterface
{
    private BaseViewFactory $viewFactory;

    public function __construct(string $viewsDir, ContainerInterface $container, array $pluginClasses = [])
    {
        $plugins = [];
        foreach ($pluginClasses as $pluginClass) {
            $plugins[$pluginClass] = $container->get($pluginClass);
        }
        $this->viewFactory = new BaseViewFactory($viewsDir, ...$plugins);
    }

    public function createView(string $path): ViewInterface
    {
        return $this->viewFactory->createView($path);
    }
}
