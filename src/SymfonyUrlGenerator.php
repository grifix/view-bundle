<?php

declare(strict_types=1);

namespace Grifix\ViewBundle;

use Grifix\View\UrlGenerator\UrlGeneratorInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface as SymfonyUrlGeneratorInterface;

final class SymfonyUrlGenerator implements UrlGeneratorInterface
{

    public function __construct(private readonly SymfonyUrlGeneratorInterface $generator)
    {
    }

    public function generateUrl(string $alias, array $params = []): string
    {
        return $this->generator->generate($alias, $params);
    }
}
